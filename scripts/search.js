const opcionNavegacion = document.querySelector('.header__options');

opcionNavegacion.addEventListener('click', (evento) => {
    let elemento = event.target;

    while ( elemento.getAttribute('class') != "header__options-option" ){
        elemento = elemento.parentElement;
    }
    
    if( elemento.getAttribute('class') != "button" ) {
        for( const child of opcionNavegacion.children ) {
            child.classList.remove('selected');
        }
        elemento.classList.add('selected');
    }

})