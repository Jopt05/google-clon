const inputBuscador = document.querySelector('.searcher__input-input-div-input'),
opcionesBuscador    = document.querySelector('.searcher__input-options'),
botonesBuscador     = document.querySelectorAll('.searcher__buttons'),
btnBorrar           = document.querySelector('.x');

inputBuscador.addEventListener('focus', () => {
    opcionesBuscador.classList.remove('hidden');
    botonesBuscador[0].classList.remove('hidden');
    botonesBuscador[1].classList.add('hidden');
})

inputBuscador.addEventListener('blur', () => {
    opcionesBuscador.classList.add('hidden');
    botonesBuscador[0].classList.add('hidden');
    botonesBuscador[1].classList.remove('hidden');
    btnBorrar.classList.add('hidden');
})

inputBuscador.addEventListener('keyup', () => {
    btnBorrar.classList.remove("hidden");
})

btnBorrar.addEventListener('click', () => {
    inputBuscador.value = "";
})