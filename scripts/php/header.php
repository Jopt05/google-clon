<header class="header">
        <div class="header__search">
            <a href="./index.php"><img src="./assets/images/source1.png" alt="" class="mr header__search-logo"></a>
            <form class="header__search-input-div" method="GET" action="./search.php">
                <input type="text" class="mr header__search-input-div-input" name="buscador">
                <i class='mr bx bxs-microphone'></i>
                <i class='bx bx-search'></i>
            </form>
            <div class="header__search-user">
                <div class="mr header__search-user-menu">
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                    <span class="header__search-user-menu-square"></span>
                </div>
                <img src="./assets/images/source2.png" alt="user" class="header__img">
            </div>
        </div>
        <div class="header__options">
            <div class="header__options-option selected">
                <i class='mr bx bx-search'></i>
                <p class="header__options-option-text">Todos</p>
            </div>
            <div class="header__options-option">
                <i class='mr bx bxs-videos'></i>
                <p class="header__options-option-text">Videos</p>
            </div>
            <div class="header__options-option">
                <i class='mr bx bx-news' ></i>
                <p class="header__options-option-text">Noticias</p>
            </div>
            <div class="header__options-option">
                <i class='mr bx bx-current-location' ></i>
                <p class="header__options-option-text">Maps</p>
            </div>
            <div class="header__options-option">
                <i class='mr bx bxs-image-alt' ></i>
                <p class="header__options-option-text">Imagenes</p>
            </div>
            <div class="header__options-option button">
                <i class='mr bx bx-dots-vertical-rounded' ></i>
                <p class="header__options-option-text">Más</p>
            </div>
            <div class="header__options-option button">
                <p class="header__options-option-text">Preferencias</p>
            </div>
        </div>
    </header>