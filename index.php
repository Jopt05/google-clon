<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    
    <link rel="stylesheet" href="./styles/index.css">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="icon" href="./assets/images/source3.ico" type="image/icon type">

    <title>Google Clone</title>
</head>
<body>
    <div class="header">
        <p class="mr header__text">Gmail</p>
        <p class="mr header__text">Imágenes</p>
        <div class="mr header__menu">
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
            <span class="header__menu-square"></span>
        </div>
        <img src="./assets/images/source2.png" alt="user" class="header__img">
    </div>
    <form class="searcher" method="GET" action="./search.php">
        <img src="./assets/images/source1.png" alt="logo" class="searcher__logo">
        <div class="searcher__input">
            <div class="searcher__input-div">    
                <i class='mr bx bx-search'></i>
                <input type="text" class="searcher__input-input-div-input" name="buscador">
                <i class='hidden mr x bx bx-x'></i>
                <i class='bx bxs-microphone'></i>
            </div>
            <div class="hidden searcher__input-options">
                <!-- <div class="searcher__input-options-option">
                    <i class='bx bxs-watch'></i>
                    <p class="searcher__input-options-option-text">Elemento 1</p>
                </div> -->
            </div>
            <div class="hidden searcher__buttons">
                <button type="submit" class="mr searcher__buttons-button">Buscar con Google</button>
                <button type="submit" class="searcher__buttons-button">Me siento con suerte</button>
            </div>
        </div>
        <div class="searcher__buttons">
            <button type="submit" class="mr searcher__buttons-button">Buscar con Google</button>
            <button type="submit" class="searcher__buttons-button">Me siento con suerte</button>
        </div>
    </form>
    <script src="./scripts/components.js"></script>
</body>
</html>