<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buscar con Google</title>

    <link rel="stylesheet" href="./styles/search.css">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="icon" href="./assets/images/source3.ico" type="image/icon type">

</head>
<body>
    <?php include "./scripts/php/header.php"; ?>
    <div class="results">
        <!-- <div class="results__result">
            <a href="" class="results__result-link">Resultado Numero 1</a>
            <p class="results__result-text">Allá en la fuente había un chorrito se hacía
                grandote se hacía chiquito estaba de mal humor pobre chorrito tenía calor.
                Ahí va la hormiga...
            </p>
        </div> -->
        <?php include "./scripts/php/searchTerm.php"; ?>
    </div>
    <script src="./scripts/search.js"></script>
</body>
</html>